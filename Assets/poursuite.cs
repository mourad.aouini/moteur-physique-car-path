using UnityEngine;
//**********************************************
// H.AJLANI-ESPRIT-5GamiX- 2022-2023
//**********************************************
public class poursuite : MonoBehaviour
{
    // Start is called before the first frame update
    private Vector2 vel;
    private Vector2 pos;
    //private Vector2 vel;
    private Vector2 direction;
    private Vector2 obtacledirection;
    private Vector2 normalizeddirection;
    public float rotspeed;
    float vmax = 0.4f;
    float radius = 2.0f;
    float tau = 3.0f;
    Quaternion rotangle;
    public Transform[] path;
    public Transform[] obstacles;
    public int node = 0;
    public int i = 0;
    public Transform obstacle;
    void Start()
    {
        vel = Vector2.zero;
        pos = transform.position;
        normalizeddirection = Vector2.zero;


    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(transform.position, transform.up + transform.right, Color.red);
        Debug.DrawRay(transform.position, transform.up - transform.right, Color.red);

        
        Vector2 distance = new Vector2(path[node].position.x, path[node].position.y);
        Vector2 distance2 = new Vector2(obstacles[i].position.x, obstacles[i].position.y);
        float deltaT = Time.fixedDeltaTime;

        direction = distance - pos;

        obtacledirection = distance2 - pos;

        float f = Vector3.Angle(transform.up, transform.up- transform.right);
        Debug.Log(f);
        // orienter l objet vers sa cible------------------

        float betadegree = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90;
        rotangle = Quaternion.AngleAxis(betadegree, new Vector3(0, 0, 1));

        //-------------------------------------------------
        //detecter si l'obstacle est dans la distance et l'angle de detection 
        //-------------------------------------------------


        if (obtacledirection.magnitude <= 1 && Vector2.Dot(obtacledirection.normalized, transform.up) > Mathf.Cos(90 * 0.5f * Mathf.Deg2Rad))
        {
            avoidance();
        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, rotangle, Time.deltaTime * 2.0f);
            movement(direction);
        }
        nodes();
        obstacl();

    }

    public void avoidance()
    {
        if (Vector2.Dot(obtacledirection.normalized, transform.up) >= Vector2.Dot(obtacledirection.normalized, transform.up - transform.right))
        {
            Debug.Log("turnleft");
            transform.eulerAngles += new Vector3(0, 0, 1) * Time.deltaTime * rotspeed;

        }
        else
        {
            Debug.Log("turnright");
            transform.eulerAngles += new Vector3(0, 0, -1) * Time.deltaTime * rotspeed;
        }

        movement(transform.up);

        if (obtacledirection.magnitude <= radius) //Ralentissement si en zone critique
        {
            vel = vel / tau;

        }
    }

    public void movement(Vector3 direction)
    {
        vel = direction;
        vel.Normalize(); // velocity is now normalized
        vel = vmax * vel;
        pos += vel * Time.fixedDeltaTime;
        transform.position = pos;
    }

    public void nodes()
    {
        if (Vector2.Distance(transform.position, path[node].position) < 0.7f)
        {
            if (node == path.Length - 1)
            {
                node = 0;
            }
            else
            node++;
        }
    }
    public void obstacl()
    {
        if (Vector2.Distance(transform.position, obstacles[i].position) < 0.7f)
        {
            if (i == obstacles.Length - 1)
            {
                i = 0;
            }
            else
            i++;
        }
    }
}
